class Move < ActiveRecord::Base
  rescue_from_duplicate :game_id, scope: [:cord_x, :cord_y]
  extend Enumerize
  enumerize :sign, in: [:x, :o]
  belongs_to :game, required: true

  def to_s
    [cord_x, cord_y].join(',')
  end

  def to_a
    [cord_x, cord_y]
    end

    def to_h
      Hash[to_s, sign]
    end
end
