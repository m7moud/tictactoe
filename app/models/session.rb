class Session < ActiveRecord::Base
  validates :player1, :player2, presence: true
  has_one :current_game, class_name: 'Game', foreign_key: :id, primary_key: :current_game_id, autosave: false
  has_many :games
  after_initialize do
    self.uuid = SecureRandom.uuid unless uuid
  end
  after_create do
    if current_game_id.nil?
      self.current_game_id = Game.create(session_id: id).id
      save
    end
  end

  def info
    current_sign = (current_game.moves ? 'x' : (%w(x o) - [current_game.moves.order(:created_at).last.sign]).first)
    {
      players: [
        {
          name: player1,
          score: player1_win
        },
        {
          name: player2,
          score: player2_win
        }
      ],
      draws: draws,
      moves: current_game.moves.map(&:to_h),
      current_move: {
        player: send(current_sign.eql?('x') ? current_game.sign_x : (%w(player1 player2) - [current_game.sign_x]).first),
        sign: current_sign
      }
    }
  end

  def new_game(current_game_winner = nil)
    if current_game_winner.nil?
      self.draws += 1
    else
      self[current_game.winner + '_win'] += 1
    end
    self.current_game_id = Game.create(
      session_id: id, sign_x: (%w(player1 player2) - [current_game.sign_x]).first
    ).id
    save
    reload
    self
  end
end
