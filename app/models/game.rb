class Game < ActiveRecord::Base
  extend Enumerize
  enumerize :sign_x, in: { player1: 1, player2: 2 }, default: :player1
  enumerize :winner, in: { player1: 1, player2: 2 }
  belongs_to :session, required: true
  has_many :moves

  def winner?
    return nil if moves.size < 5 # Can't tell yet

    board = moves_matrix
    board_rotated = board.transpose.map(&:reverse)

    (
      board + # Row
      board_rotated + # Column
      [(0..2).collect { |i| board[i][i] }] + # Diagonal
      [(0..2).collect { |i| board_rotated[i][i] }] # Rotated diagonal
    ).each do |subset|
      uniq = subset.uniq
      return uniq.first if uniq.size.eql?(1) and !uniq.eql?([nil])
    end

    # Draw
    nil
  end

  def moves_matrix
    moves.inject(Array.new(3) { Array.new(3) }) do |board, move|
      board[move.cord_x][move.cord_y] = move.sign
      board
    end
  end
end
