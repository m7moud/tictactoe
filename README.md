# Tic Tac Toe
Simple Tic Tac on WebSocket, React on ES6, and Ruby.

Both players can play it on a the same window, and it can easily be converted to a real-time remote game.

You can find it deployed in here: [https://secure-cove-4753.herokuapp.com](https://secure-cove-4753.herokuapp.com)

## Dependencies Installation
`bundle install`

`rake db:migrate`

## Usage

```
bundle exec rackup config.ru -s thin -p 9292
```

or if you wish to build the assets again, use:

```
foreman start -f Procfile.dev
```

then head to: [http://localhost:9292/](http://localhost:9292/)

## License
Copyright (c) 2015 by Mahmoud Ali
