require './environment'

TicTacToe = lambda do |env|
  ActiveRecord::Base.logger = Logger.new(STDOUT) if ENV['RACK_ENV'].eql?('development')
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env)
    ws.on :open do |event|
      uuid = event.current_target.env['PATH_INFO'][1..-1]
      @session = Session.includes(:current_game).references(:current_game).find_by_uuid(uuid)
      unless @session
        ws.send(['error', { errors: ["Session doesn't exist"] }].to_json)
        ws.close
      end
    end

    ws.on :message do |event|
      if event.data.eql?('ping!')
        ws.send('pong')
      else
        message = JSON.parse(event.data)
        case message[0]
        when 'getinfo'
          ws.send(['info', @session.info].to_json)
        when 'move'
          @session.current_game.moves << Move.new(cord_x: message[1]['cord'][0], cord_y: message[1]['cord'][1], sign: message[1]['sign'])
          if @session.current_game.moves.size > 4
            winner = @session.current_game.winner?
            if winner
              @session.current_game.update(winner: winner.eql?('x') ? @session.current_game.sign_x : (%w(player1 player2) - [@session.current_game.sign_x]).first)
              ws.send(['winner', { player: @session[@session.current_game.winner] }].to_json)
              @session.new_game(winner)
            elsif @session.current_game.moves.size.eql?(9) # It's a draw
              ws.send(['winner', { player: nil }].to_json)
              @session.new_game
            end
          end
        end
      end
    end

    ws.on :close do |_event|
      ws = nil
      @session = nil
    end

    # Return async Rack response
    ws.rack_response
  else
    # Normal HTTP request
    req = Rack::Request.new(env)
    if req.get? && !req.path.start_with?('/assets') # Serve HTML5 requests
      [200, { 'Content-Type' => 'text/html', 'Cache-Control' => 'public, max-age=86400' }, File.open('public/index.html', File::RDONLY)]
    elsif req.get? # Serve GET requests
      Rack::Static.new(self,
                       urls: ['/assets'],
                       root: 'public').call(env)
    elsif req.post? && req.path.sub(/(\/)+$/, '').eql?('/sessions/new') # Serve POST /sessions/new
      @session = Session.create(player1: req.params['player1'], player2: req.params['player2'])
      if @session.persisted?
        [200, { 'Content-Type' => 'application/json' }, [{ success: true, session: { name: "#{req.params['player1']} vs. #{req.params['player2']}", uuid: @session.uuid } }.to_json]]
      else
        [422, { 'Content-Type' => 'application/json' }, [{ success: false, errors: @session.errors.to_hash.merge(full_messages: @session.errors.full_messages) }.to_json]]
      end
    end
  end
end
