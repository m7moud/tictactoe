class CreateSessions < ActiveRecord::Migration
  def up
    create_table :sessions do |t|
      t.string :uuid
      t.references :current_game
      t.string :player1
      t.string :player2
      t.integer :player1_win, default: 0
      t.integer :player2_win, default: 0
      t.integer :draws, default: 0
      t.timestamps null: false
    end
    add_index :sessions, :uuid, unique: true
    add_index :sessions, :current_game_id
  end

  def down
    drop_table :sessions
  end
end
