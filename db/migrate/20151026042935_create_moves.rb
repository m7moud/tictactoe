class CreateMoves < ActiveRecord::Migration
  def up
    create_table :moves do |t|
      t.references :game
      t.string :sign, limit: 1
      t.integer :cord_x, limit: 1
      t.integer :cord_y, limit: 1
      t.timestamp :created_at, null: false
    end
    add_index :moves, :game_id
    add_index :moves, [:game_id, :cord_x, :cord_y], unique: true
  end

  def down
    drop_table :moves
  end
end
