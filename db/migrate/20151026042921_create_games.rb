class CreateGames < ActiveRecord::Migration
  def up
    create_table :games do |t|
      t.references :session
      t.integer :sign_x, limit: 1
      t.integer :winner
      t.timestamps null: false
    end
    add_index :games, :session_id
    add_index :games, :winner
  end

  def down
    drop_table :games
  end
end
