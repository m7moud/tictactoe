# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151026042935) do

  create_table "games", force: :cascade do |t|
    t.integer  "session_id"
    t.integer  "sign_x",     limit: 1
    t.integer  "winner"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "games", ["session_id"], name: "index_games_on_session_id"
  add_index "games", ["winner"], name: "index_games_on_winner"

  create_table "moves", force: :cascade do |t|
    t.integer  "game_id"
    t.string   "sign",       limit: 1
    t.integer  "cord_x",     limit: 1
    t.integer  "cord_y",     limit: 1
    t.datetime "created_at",           null: false
  end

  add_index "moves", ["game_id", "cord_x", "cord_y"], name: "index_moves_on_game_id_and_cord_x_and_cord_y", unique: true
  add_index "moves", ["game_id"], name: "index_moves_on_game_id"

  create_table "sessions", force: :cascade do |t|
    t.string   "uuid"
    t.integer  "current_game_id"
    t.string   "player1"
    t.string   "player2"
    t.integer  "player1_win",     default: 0
    t.integer  "player2_win",     default: 0
    t.integer  "draws",           default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "sessions", ["current_game_id"], name: "index_sessions_on_current_game_id"
  add_index "sessions", ["uuid"], name: "index_sessions_on_uuid", unique: true

end
