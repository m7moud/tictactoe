import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, Link, History, Lifecycle } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'
let history = createBrowserHistory()

import App from './components/app.react'
import Lobby from './components/lobby.react'
import Board from './components/board.react'


render((
	<Router history={history}>
		<Route path="/" component={App}>
			<IndexRoute component={Lobby}/>
	    <Route path="/board/:uuid" component={Board}/>
		</Route>
  </Router>
), document.getElementById('react-container'))
