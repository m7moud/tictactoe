import React from 'react'
import { Alert } from 'react-bootstrap'

class FlashMessages extends React.Component {
  render() {
    return (
      <Alert
        bsStyle="danger"
        dismissAfter={4000}
        onDismiss={this.handleAlertDismiss}>
        <ul className="list-unstyled">
          {this.props.errors.map((error, i) => {
            return (
              <li key={i}>
                {error}
              </li>
            )
          })}
        </ul>
      </Alert>
    )
  }
}

export default FlashMessages
