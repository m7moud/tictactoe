import React from 'react'
import { Input, Button } from 'react-bootstrap'
import RecentSessions from './recent_sessions.react'
import FlashMessages from './flash_messages.react'
import $ from 'jquery'

class Lobby extends React.Component {
  constructor() {
    super()
    this.state = {
      errors: [],
      history: [],
      alertVisible: false
    }
    if (localStorage.history) {
      this.state.history = JSON.parse(localStorage.getItem('history'))
    }
    this.startSession = this.startSession.bind(this)
    this.handleAlertDismiss = this.handleAlertDismiss.bind(this)
  }
  startSession(e) {
    e.preventDefault()
    var player1 = this.refs.player1.getValue().trim()
    var player2 = this.refs.player2.getValue().trim()
    $.ajax({
      url: "/sessions/new",
      dataType: 'json',
      type: 'POST',
      data: {
        player1: player1,
        player2: player2
      },
      success: function(data) {
        this.setState({
          alertVisible: false
        })
        this.state.history.unshift(data.session)
        localStorage.setItem('history', JSON.stringify(this.state.history.slice(0, 3)))
        this.props.history.pushState(null, `/board/${data.session.uuid}`)
      }.bind(this),
      error: function(xhr, status, err) {
        if(xhr.responseJSON){
          this.setState({
            errors: xhr.responseJSON.errors.full_messages,
            alertVisible: true
          })
        }
      }.bind(this)
    })
  }
  handleAlertDismiss() {
    this.setState({
      alertVisible: false,
      errors: []
    })
  }
  render() {
    var alerts, history
    if (this.state.alertVisible) {
      alerts = <FlashMessages {...this.state}/>
    }
    if (this.state.history.length > 0) {
      history = <RecentSessions {...this.state} />
    }
    return (
      <div>
        <h2>
          Welcome to Tic Tac Toe
        </h2>
        <div className="row">
          <div className="col-md-8">
            {alerts}
            <div className="panel panel-default">
              <div className="panel-body">
                <p>
                  Enter the player names please to start a new game
                </p>
                <form onSubmit={this.startSession}>
                  <div className="form-group">
                    <Input
                      autoFocus="true"
                      placeholder="Player1"
                      ref="player1"
                      type="text"/>
                  </div>
                  <div className="form-group">
                    <Input
                      placeholder="Player2"
                      ref="player2"
                      type="text"/>
                  </div>
                  <Button bsStyle="primary" type="submit" block>Start</Button>
                </form>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            {history}
          </div>
        </div>
      </div>
    )
  }
}

export default Lobby
