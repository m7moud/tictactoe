import React from 'react'
import { Button } from 'react-bootstrap'

class GameResult extends React.Component {
  render() {
    return (
      <span>
        {this.props.message}
        <div>
          <Button
            className="btn btn-primary"
            onClick={this.props.newGame}>
            New Game
          </Button>
        </div>
      </span>
    )
  }
}

export default GameResult
