import React from 'react'
import Header from './header.react'
import Footer from './footer.react'

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        {this.props.children}
        <Footer />
      </div>
    )
  }
}

export default App
