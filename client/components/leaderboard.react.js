import React from 'react'

class LeaderBoard extends React.Component {
  render() {
    return (
      <div className="well well-sm">
        <h4>Leaderboard</h4>
        <ol>
          {this.props.players.sort((x, y) => {
            return x.score > y.score ? -1 : x.score < y.score ? 1 : 0
          }).map((player, i) => {
            return (
              <li key={i} className="player">
                {player.name}
                <span className="label label-primary pull-right">
                  {player.score}
                </span>
              </li>
            )
          })}
        </ol>
      </div>
    )
  }
}

export default LeaderBoard
