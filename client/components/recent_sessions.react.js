import React from 'react'
import { Link } from 'react-router'

class RecentSessions extends React.Component {
  render() {
    return (
      <div className="well well-sm">
        <h4>
          Recent Sessions
        </h4>
        <ul className="nav nav-pills nav-stacked">
          {this.props.history.map(function (session, i) {
            return (
              <li key={session.uuid}>
                <Link to={`/board/${session.uuid}`}>
                  {session.name}
                </Link>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default RecentSessions
