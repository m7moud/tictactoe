import React from 'react'
import { IndexLink } from 'react-router'
import {Navbar, NavBrand} from 'react-bootstrap'

class Header extends React.Component {
  render() {
    return (
      <Navbar>
        <NavBrand>
          <IndexLink to="/">
            <img src="/assets/images/logo.png"/>
          </IndexLink>
        </NavBrand>
      </Navbar>
    )
  }
}

export default Header
