import React from 'react'
import { ReconnectingWebSocket } from 'awesome-websocket'
import LeaderBoard from './leaderboard.react'
import GameResult from './game_result.react'
import FlashMessages from './flash_messages.react'
import $ from 'jquery'

class Board extends React.Component {
  constructor() {
    super()
    this.state = {
      connected: false,
      players: [],
      current_move: {},
      moves: {},
      overlayMessages: [],
      errors: []
    }
    this.applyMove = this.applyMove.bind(this)
    this.newGame = this.newGame.bind(this)
  }
  componentWillMount() {
    this.soundElements = {"x": new Audio('/assets/sound/beep1.ogg'), "o": new Audio('/assets/sound/beep2.ogg')}
    this.socket = new ReconnectingWebSocket(location.origin.replace(/^http/, 'ws') + '/' + this.props.params.uuid)

    this.socket.onopen = () => {
      this.setState({
        connected: true,
        overlayMessages: this.state.overlayMessages.filter(e => e!=='Disconnected')
      })
      this.socket.keepAlive(60 * 1000, "ping!") // just to keep it live
      if (this.state.overlayMessages.filter(e => e!=='Disconnected').length === 0) {
        this.socket.send(['getinfo', {}])
      }
    }

    this.socket.onmessage = (e) => {
      if (e.data !== 'pong') {
        var data = JSON.parse(e.data)
        switch (data[0]) {
        case "info" :
          this.setState({
            moves: data[1]["moves"].reduce(function(o, v, i){o[Object.keys(v)[0]] = v[Object.keys(v)[0]]
              return o
            }, {}),
            players: data[1]['players'],
            current_move: data[1]['current_move']
          })
          break
        case "winner" :
          var overlayMessages = this.state.overlayMessages
          var toSet = {}
          if (data[1]["player"] === null) {
            overlayMessages.unshift(<GameResult message="It's a draw" newGame={this.newGame}/>)
          } else {
            overlayMessages.unshift(<GameResult message={data[1]["player"] + " is the winner"} newGame={this.newGame}/>)
            toSet["players"] = this.state.players.map(player => {
              if (player.name === data[1]["player"])
                player.score += 1
              return player
            })
          }
          toSet["overlayMessages"] = overlayMessages
          this.setState(toSet)
          break
        case "error" :
          this.setState({
            errors: data[1]['errors']
          })
          this.socket.close()
          break
        }
      }
    }

    this.socket.onerror = (err) => {
      var overlayMessages = this.state.overlayMessages
      overlayMessages.unshift('Disconnected')
      this.setState({
        connected: false,
        overlayMessages: overlayMessages
      })
    }
  }
  componentWillUnmount() {
    this.socket.close()
  }
  applyMove(e) {
    var moves = this.state.moves
    if (this.state.connected && typeof moves[$(e.target).data('cord')] === 'undefined') {
      this.soundElements[this.state.current_move.sign].play()
      moves[$(e.target).data('cord')] = this.state.current_move.sign
      $(e.target).addClass('sign_' + this.state.current_move.sign).removeClass('empty')
      this.setState({
        moves: moves,
        current_move: {
          player: this.state.players.filter(player => player.name !== this.state.current_move.player)[0].name,
          sign: (this.state.current_move.sign === 'x' ? 'o' : 'x')
        }
      })
      this.emitMove(this.state.current_move.sign, $(e.target).data('cord').split(","))
    }
  }
  emitMove(sign, cord) {
    this.socket.send(['move', {
          sign: sign,
          cord: cord
        }
      ])
  }
  newGame(e) {
    e.preventDefault()
    this.socket.send(['getinfo', {}])
    this.setState({
      overlayMessages: this.state.overlayMessages.filter(e => e[0]==='<')
    })
  }
  render() {
    if (Object.keys(this.state.errors).length > 0) {
      return (
        <FlashMessages {...this.state}/>
      )
    }
    if (Object.keys(this.state.current_move).length === 0) {
      return (
        <h3>
          Loading ...
        </h3>
      )
    }
    var overlay
    if (this.state.overlayMessages.length) {
      overlay = <div className="overlay text-center"><span>{this.state.overlayMessages[0]}</span></div>
    }
    return (
      <div className="row">
        <div className="col-md-8 board">
          <div className="play-board center-block">
            {overlay}
            {[[...Array(3)], [...Array(3)], [...Array(3)]].map((a, x) =>
              a.map((i, y) =>
              <div
                className={`${typeof this.state.moves[`${x},${y}`] === "undefined" ? "empty" : `sign_${this.state.moves[`${x},${y}`]}`}` + " cell"}
                key={x + y}
                data-cord={`${x},${y}`}
                ref={`cell_${x},${y}`}
                onClick={this.applyMove}>
              </div>
            ))}
          </div>
        </div>
        <div className="col-md-4">
          <div className="alert alert-info">
            It's your turn <strong>{this.state.current_move.player}</strong>
          <div className={"pull-right label-sign sign_" + `${this.state.current_move.sign}`}>
          </div>
        </div>
        <LeaderBoard {...this.state}/>
      </div>
    </div>
    )
  }
}

export default Board
