require 'bundler'
Bundler.require

StandaloneMigrations::Configurator.load_configurations
ActiveRecord::Base.establish_connection

Dir[File.expand_path('../app/models/*.rb', __FILE__)].each do |file|
  require file
end

Time.zone = "UTC"
